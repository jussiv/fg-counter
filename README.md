# FG-counter

FG-counter is an amateur project aimed to design and implement a cheap solution 
for visitor counting in remote locations off the electric grid. The 
project is built on [Texas Instruments](http://www.ti.com/) 
[LaunchPad](http://www.ti.com/launchpad) 
[MSP-EXP430G2](http://processors.wiki.ti.com/index.php/MSP430_LaunchPad_%28MSP-EXP430G2%29) 
development board but is also designed to be usable with only the IC and a custom PCB for it.

## Project background

The project was started when my disc golf club [Jyli](http://jyli.fi) built their 
own disc golf course and later on began wondering how much traffic the course was 
getting. With a little research we found out that commercial counting solution 
prices ranged from 500 euros upwards so it was not an option. A little more 
research and I decided to build a counter on my own. The targets 
and limits of the project are

*	Low cost
*	Low power consumption
*	Ability to withstand Finnish weather around the year
*	User-friendliness

## Project tools

The project will be designed using [KiCad](http://www.kicad-pcb.org/) for its 
open-source code base and available support from my acquaintances.
As I have no previous experience of Git, electronics, programming microcontrollers 
or even designing electric circuits any advice is more than welcome even if I 
don't ask for it. Just keep in mind that you'll need to fashion your advices in 
a 'for dummies' format as this is all doing-while-learning for me. If you wish 
to commit to the project feel free to drop me a line and let's see what you can 
offer.
